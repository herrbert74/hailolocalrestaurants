package com.herrbert74.hailolocalrestaurants.entities;

public class Restaurant implements Comparable<Restaurant>{

	double latitude;
	double longitude;
	String name;
	float rating;
	String vicinity;
	int distanceFromCurrentLocation;

	/**
	 * The Restaurant class
	 * 
	 * Holds the data for each restaurant.
	 * Comparable on the distance from the user, so it can be sorted on this distance. 
	 * 
	 * @param latitude
	 * @param longitude
	 * @param name
	 * @param rating
	 * @param vicinity
	 */
	
	public Restaurant(double latitude, double longitude, String name, float rating, String vicinity){
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.rating = rating;
		this.vicinity = vicinity;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String getVicinity() {
		return vicinity;
	}

	public void setVicinity(String vicinity) {
		this.vicinity = vicinity;
	}

	public int getDistanceFromCurrentLocation() {
		return distanceFromCurrentLocation;
	}

	public void setDistanceFromCurrentLocation(int distanceFromCurrentLocation) {
		this.distanceFromCurrentLocation = distanceFromCurrentLocation;
	}

	@Override
	public int compareTo(Restaurant another) {
		return this.distanceFromCurrentLocation - another.getDistanceFromCurrentLocation();
	}
	
	

}
