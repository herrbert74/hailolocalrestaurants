package com.herrbert74.hailolocalrestaurants.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.osmdroid.util.GeoPoint;

import android.util.Log;

import com.herrbert74.hailolocalrestaurants.HailoApp;
import com.herrbert74.hailolocalrestaurants.entities.Restaurant;

/**
 * The Class JSONHelper.
 * 
 * Contains helper methods for JSON processing.
 */
public class JSONHelper {

	/**
	 * The listener interface for receiving createRestaurantList events. The
	 * class that is interested in processing a createRestaurantList event
	 * implements this interface, and the object created with that class is
	 * registered with a component using the component's
	 * <code>addCreateRestaurantListListener<code> method. When
	 * the createRestaurantList event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see CreateRestaurantListEvent
	 */
	public interface CreateRestaurantListListener {

		/**
		 * Parsing finished.
		 * 
		 * @param restaurants
		 *            the restaurants
		 */
		public void parsingFinished(ArrayList<Restaurant> restaurants);
	};

	/**
	 * The listener interface for receiving JSONParser events. The class that is
	 * interested in processing a JSONParser event implements this interface,
	 * and the object created with that class is registered with a component
	 * using the component's <code>addJSONParserListener<code> method. When
	 * the JSONParser event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see JSONParserEvent
	 */
	public interface JSONParserListener {

		/**
		 * Parsing finished.
		 * 
		 * @param array
		 *            the array
		 */
		public void parsingFinished(Object array);

		/**
		 * Parsing failed.
		 * 
		 * @param ex
		 *            the ex
		 */
		public void parsingFailed(Exception ex);
	}

	/**
	 * Parses a JSON array.
	 * 
	 * @param restaurantJSONList
	 *            the restaurant json list
	 * @param _listener
	 *            the _listener
	 */
	public static void parseJSONArray(String restaurantJSONList, final JSONParserListener _listener) {
		final JSONTokener tokener = new JSONTokener(restaurantJSONList);
		// Object html_attributions = null;
		// Object results = null;
		try {
			JSONObject html_attributions = (JSONObject) tokener.nextValue();
			JSONArray results = html_attributions.getJSONArray("results");

			if (results instanceof JSONArray)
				_listener.parsingFinished((JSONArray) results);
			else {
				_listener.parsingFailed(new Exception("Command failed!"));
			}

		} catch (JSONException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Creates the restaurant list 
	 * 
	 * 
	 * Creates the list and passes it back to the caller through a listener interface.
	 * 
	 * @param restaurantJSONList
	 *            the restaurant JSON list
	 * @param _listener
	 *            the _listener
	 */
	public static void createList(String restaurantJSONList, final CreateRestaurantListListener _listener) {
		JSONParserListener jsonParserListener = new JSONParserListener() {

			@Override
			public void parsingFinished(Object array) {
				JSONArray jArray = (JSONArray) array;
				ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
				GeoPoint currentLocation = HailoApp.getCurrentLocation();

				for (int i = 0; i < jArray.length(); i++) {
					double latitude = 0d;
					double longitude = 0d;
					float rating = 0f;
					String name = "";
					String vicinity = "";
					try {
						JSONObject json_data = jArray.getJSONObject(i);
						JSONObject geometry = json_data.getJSONObject("geometry");
						JSONObject location = geometry.getJSONObject("location");
						latitude = location.getDouble("lat");
						longitude = location.getDouble("lng");
						name = json_data.getString("name");
						vicinity = json_data.getString("vicinity");
						rating = Float.valueOf(json_data.getString("rating"));

					} catch (JSONException e) {
						Log.e("log_tag", "Error parsing data " + e.toString());
					}
					Restaurant restaurant = new Restaurant(latitude, longitude, name, rating, vicinity);
					int distanceFromCurrentLocation = currentLocation.distanceTo(new GeoPoint(latitude, longitude));
					restaurant.setDistanceFromCurrentLocation(distanceFromCurrentLocation);
					restaurants.add(restaurant);
				}
				Collections.sort(restaurants);
				_listener.parsingFinished(restaurants);

			}

			@Override
			public void parsingFailed(Exception ex) {
				// TODO Auto-generated method stub

			}
		};
		parseJSONArray(restaurantJSONList, jsonParserListener);
	}

	/**
	 * Creates the save file.
	 * 
	 * Saves the JSON data unchanged into the file system.
	 * 
	 * @param params
	 *            the params
	 * @param mJsonResponse
	 *            the JSON response to save
	 */
	public static void createAndSaveFile(String params, String mJsonResponse) {
		try {
			FileWriter file = new FileWriter(HailoApp.getContext().getFilesDir().getPath() + "/" + params);
			file.write(mJsonResponse);
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read JSON data from the file system.
	 * 
	 * @param params
	 *            the params
	 * @return the string
	 */
	public static String readJsonData(String params) {
		String mResponse = "";
		try {
			File f = new File(HailoApp.getContext().getFilesDir().getPath() + "/" + params);
			FileInputStream is = new FileInputStream(f);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			mResponse = new String(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mResponse;
	}
}
