package com.herrbert74.hailolocalrestaurants.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.herrbert74.hailolocalrestaurants.HailoApp;
import com.herrbert74.hailolocalrestaurants.activities.HailoMapActivity;
import com.herrbert74.hailolocalrestaurants.entities.Restaurant;
import com.herrbert74.hailolocalrestaurants.R;

/**
 * The Class RestaurantListAdapter.
 */
public class RestaurantListAdapter extends BaseAdapter {

	/** The values. */
	private final ArrayList<Restaurant> values;

	/**
	 * Instantiates a new restaurant list adapter.
	 *
	 * @param values the values
	 */
	public RestaurantListAdapter(ArrayList<Restaurant> values) {

		this.values = values;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		TextView lbl_name;
		TextView lbl_vicinity;
		TextView lbl_location;
		TextView lbl_rating;
		TextView lbl_distance;

		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.listitem_restaurants, parent, false);
		}
		lbl_name = (TextView) convertView.findViewById(R.id.lbl_name);
		lbl_vicinity = (TextView) convertView.findViewById(R.id.lbl_vicinity);
		lbl_location = (TextView) convertView.findViewById(R.id.lbl_location);
		lbl_rating = (TextView) convertView.findViewById(R.id.lbl_rating);
		lbl_distance = (TextView) convertView.findViewById(R.id.lbl_distance);

		lbl_name.setTypeface(HailoApp.getFontHeaders());
		lbl_vicinity.setTypeface(HailoApp.getFontButtons());
		lbl_location.setTypeface(HailoApp.getFontButtons());
		lbl_rating.setTypeface(HailoApp.getFontButtons());
		lbl_distance.setTypeface(HailoApp.getFontButtons());		
		
		lbl_name.setText(values.get(position).getName());
		lbl_vicinity.setText(values.get(position).getVicinity());
		String LatQuarter = values.get(position).getLatitude() > 0 ? "N" : "S";
		String LngQuarter = values.get(position).getLongitude() > 0 ? "E" : "W";
		Resources resources = HailoApp.getContext().getResources();
		lbl_location.setText(resources.getString(R.string.adapter_location) + " "
				+ values.get(position).getLatitude() + "78\u00B0" + " " + LatQuarter + " " + values.get(position).getLongitude() + "78\u00B0"
				+ " " + LngQuarter);
		lbl_rating.setText(resources.getString(R.string.adapter_rating) + " "
				+ Float.toString(values.get(position).getRating()));
		lbl_distance.setText(resources.getString(R.string.adapter_distance) + " "
				+ Float.toString(values.get(position).getDistanceFromCurrentLocation()) + " "
				+ resources.getString(R.string.adapter_meter));
		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HailoApp.getContext(), HailoMapActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				HailoApp.getContext().startActivity(intent);
				// finish();
			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		return values.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return null;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return 0;
	}
}