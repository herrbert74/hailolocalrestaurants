package com.herrbert74.hailolocalrestaurants.activities;

import java.util.ArrayList;

import org.osmdroid.util.GeoPoint;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;
import com.herrbert74.hailolocalrestaurants.HailoApp;
import com.herrbert74.hailolocalrestaurants.R;
import com.herrbert74.hailolocalrestaurants.adapters.RestaurantListAdapter;
import com.herrbert74.hailolocalrestaurants.dao.JSONHelper;
import com.herrbert74.hailolocalrestaurants.entities.Restaurant;
import com.herrbert74.hailolocalrestaurants.services.GPSTracker;

// TODO: Auto-generated Javadoc
/**
 * The Class MainActivity.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends SherlockActivity {

	// GPSTracker class
	/** The gps. */
	GPSTracker gps;
	
	/** The AQuery class */
	AQuery aq;
	
	/** The progress dialog to show the user that data is loading. */
	ProgressDialog pd;
	
	/** The offline mode switcher checkbox. */
	CheckBox chk;
	
	/** The label for offline mode. Clickable. */
	TextView lbl_offline;

	/** The listview showing the results. */
	@ViewById
	ListView lv;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aq = new AQuery(this);
		gps = new GPSTracker(MainActivity.this);
	}

	/**
	 * AfterViews method
	 * 
	 * When used with AndroidAnnotation, these assignments have to be moved from onCreate to here,
	 * because the binding of the views happens only after onCreate.
	 */
	@AfterViews
	public void av() {

		refreshAndUpdate();

		// Inflate the custom view
		View customNav = LayoutInflater.from(this).inflate(R.layout.actionbar_layout, null);

		// Bind to its state change
		chk = (CheckBox) customNav.findViewById(R.id.chk);
		chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				HailoApp.setOffline(isChecked);
			}
		});

		// Delegate click from label to checkbox
		lbl_offline = (TextView) customNav.findViewById(R.id.lbl_offline);

		lbl_offline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				chk.performClick();
			}
		});

		((Button) customNav.findViewById(R.id.btn_refresh)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				refreshAndUpdate();

			}
		});
		// Attach to the action bar
		getSupportActionBar().setCustomView(customNav);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		chk.setChecked(HailoApp.isOffline());
	}

	/**
	 * Refresh and update method.
	 * 
	 * Get GPS location and load data either from the file system (in offline mode) or
	 * from the web using Google Places API 
	 */
	public void refreshAndUpdate() {
		double latitude = 0;
		double longitude = 0;
		GeoPoint currentLocation = new GeoPoint(latitude, longitude);

		if (HailoApp.isOffline()) {
			currentLocation = HailoApp.getCurrentLocation();
			String url = "";
			String message = getResources().getString(R.string.loading);
			pd = ProgressDialog.show(MainActivity.this, message, message, true, false);
			getRestaurantData(url);

			Toast.makeText(
					HailoApp.getContext(),
					"Using your earlier location - \nLat: " + currentLocation.getLatitudeE6() + "\nLong: "
							+ currentLocation.getLongitudeE6(), Toast.LENGTH_LONG).show();
		}

		// check if GPS enabled
		else if (gps.canGetLocation()) {
			gps.getLocation();
			latitude = gps.getLatitude();
			longitude = gps.getLongitude();
			currentLocation = new GeoPoint(latitude, longitude);
			HailoApp.setCurrentLocation(currentLocation);
			String url = HailoApp.getLocationApiUrl() + Double.toString(latitude) + "," + Double.toString(longitude)
					+ HailoApp.getLocationApiParameters() + HailoApp.getLocationApiKey();
			String message = getResources().getString(R.string.loading);
			pd = ProgressDialog.show(MainActivity.this, message, message, true, false);
			getRestaurantData(url);

			Toast.makeText(HailoApp.getContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG)
					.show();
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}
	}

	/**
	 * Gets the restaurant data either from the file system or from Google Places API.
	 *
	 * @param url the url
	 * @return the restaurant data
	 */
	public void getRestaurantData(String url) {
		if (HailoApp.isOffline()) {
			offlineCallback();
		} else {
			aq.ajax(url, String.class, this, "callback");
		}
	}

	/**
	 * Offline callback.
	 * 
	 * Get data from the file system.
	 * Adds the created list data to the adapter.
	 */
	public void offlineCallback() {
		String html;
		try {
			pd.dismiss();
		} catch (NullPointerException npe) {
			Log.d("hailo", "ProgresDialog did not start");
		}
		JSONHelper.CreateRestaurantListListener createRestaurantListListener = new JSONHelper.CreateRestaurantListListener() {

			@Override
			public void parsingFinished(ArrayList<Restaurant> restaurants) {
				lv.setAdapter(new RestaurantListAdapter(restaurants));
			}
		};
		// Read offline data
		html = JSONHelper.readJsonData(HailoApp.getOfflineJsonFilename());
		JSONHelper.createList(html, createRestaurantListListener);

	}

	/**
	 * Callback.
	 * 
	 * Consumes the data from Google Places API. Saves the data for offline use
	 * and adds the created list data to the adapter.
	 *
	 * @param url the url
	 * @param html the html response
	 * @param status the status
	 */
	public void callback(String url, String html, AjaxStatus status) {
		pd.dismiss();
		JSONHelper.CreateRestaurantListListener createRestaurantListListener = new JSONHelper.CreateRestaurantListListener() {

			@Override
			public void parsingFinished(ArrayList<Restaurant> restaurants) {
				lv.setAdapter(new RestaurantListAdapter(restaurants));
			}
		};
		// Save data for offline use
		JSONHelper.createAndSaveFile(HailoApp.getOfflineJsonFilename(), html);
		JSONHelper.createList(html, createRestaurantListListener);

	}

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockActivity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	};
}
