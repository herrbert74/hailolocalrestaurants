package com.herrbert74.hailolocalrestaurants.activities;

import java.util.ArrayList;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.herrbert74.hailolocalrestaurants.HailoApp;
import com.herrbert74.hailolocalrestaurants.R;
import com.herrbert74.hailolocalrestaurants.dao.JSONHelper;
import com.herrbert74.hailolocalrestaurants.entities.Restaurant;
import com.herrbert74.hailolocalrestaurants.services.GPSTracker;

/**
 * The Class HailoMapActivity.
 * 
 * Shows a map with restaurants in a 1 mile radius.
 */
public class HailoMapActivity extends SherlockActivity {

	/** The open map view. */
	private MapView openMapView;
	
	/** The map controller. */
	private MapController mapController;
	
	/** The gps. */
	GPSTracker gps;
	
	/** The AQuery class. */
	AQuery aq;
	
	/** The pd. */
	ProgressDialog pd;
	
	/** The offline mode switcher checkbox. */
	CheckBox chk;
	
	/** The label for offline mode. Clickable. */
	TextView lbl_offline;
	
	/**
	 * Called when the activity is first created.
	 *
	 * @param savedInstanceState the saved instance state
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		aq = new AQuery(this);
		gps = new GPSTracker(HailoMapActivity.this);

		// Inflate the custom view
		View customNav = LayoutInflater.from(this).inflate(R.layout.actionbar_layout, null);

		// Bind to its state change
		chk = (CheckBox) customNav.findViewById(R.id.chk);

		chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				HailoApp.setOffline(isChecked);

			}
		});

		chk.setChecked(HailoApp.isOffline());

		// Delegate click from label to checkbox
		lbl_offline = (TextView) customNav.findViewById(R.id.lbl_offline);

		lbl_offline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				chk.performClick();
			}
		});

		((Button) customNav.findViewById(R.id.btn_refresh)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				refreshAndUpdate();

			}
		});
		// Attach to the action bar
		getSupportActionBar().setCustomView(customNav);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		openMapView = (MapView) findViewById(R.id.openmapview);
		openMapView.setBuiltInZoomControls(true);
		mapController = openMapView.getController();
		gps = new GPSTracker(HailoMapActivity.this);
				
		refreshAndUpdate();
	}
	
	/**
	 * Refresh and update method.
	 * 
	 * Get GPS location and load data either from the file system (in offline mode) or
	 * from the web using Google Places API 
	 */
	public void refreshAndUpdate() {
		double latitude = 0;
		double longitude = 0;
		GeoPoint currentLocation = new GeoPoint(latitude, longitude);
		if (HailoApp.isOffline()) {
			currentLocation = HailoApp.getCurrentLocation();
			String message = getResources().getString(R.string.loading);
			pd = ProgressDialog.show(HailoMapActivity.this, message, message, true, false);
		}
		// check if GPS enabled
		else if (gps.canGetLocation()) {
			gps.getLocation();
			latitude = gps.getLatitude();
			longitude = gps.getLongitude();
			currentLocation = new GeoPoint(latitude, longitude);
			HailoApp.setCurrentLocation(currentLocation);
			String message = getResources().getString(R.string.loading);
			pd = ProgressDialog.show(HailoMapActivity.this, message, message, true, false);
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		mapController.setZoom(14);
		mapController.setCenter(currentLocation);
		openMapView.setMultiTouchControls(true);
		String url = HailoApp.getLocationApiUrl() + Double.toString(latitude) + "," + Double.toString(longitude)
				+ HailoApp.getLocationApiParameters() + HailoApp.getLocationApiKey();
		getRestaurantData(url);

	}

	/**
	 * Gets the restaurant data either from the file system or from Google Places API.
	 *
	 * @param url the url
	 * @return the restaurant data
	 */
	public void getRestaurantData(String url) {
		if (HailoApp.isOffline()) {
			offlineCallback();
		} else {
			aq.ajax(url, String.class, this, "callback");
		}
	}

	/**
	 * Offline callback.
	 * 
	 * Get data from the file system.
	 * Adds the created list data to the map as markers.
	 */
	public void offlineCallback() {
		String html;
		JSONHelper.CreateRestaurantListListener createRestaurantListListener = new JSONHelper.CreateRestaurantListListener() {

			@Override
			public void parsingFinished(ArrayList<Restaurant> restaurants) {
				addMarkers(restaurants);
			}
		};
		// Read offline data
		html = JSONHelper.readJsonData(HailoApp.getOfflineJsonFilename());
		JSONHelper.createList(html, createRestaurantListListener);
		pd.dismiss();
	}

	/**
	 * Callback.
	 * 
	 * Consumes the data from Google Places API. Saves the data for offline use
	 * and adds the created list data to the map as markers.
	 *
	 * @param url the url
	 * @param html the html response
	 * @param status the status
	 */
	public void callback(String url, String html, AjaxStatus status) {
		// pd.dismiss();
		JSONHelper.CreateRestaurantListListener createRestaurantListListener = new JSONHelper.CreateRestaurantListListener() {

			@Override
			public void parsingFinished(ArrayList<Restaurant> restaurants) {

				addMarkers(restaurants);

			}
		};
		JSONHelper.createList(html, createRestaurantListListener);
		pd.dismiss();
	}

	/**
	 * Adds the markers to the map.
	 *
	 * @param restaurants the restaurants
	 */
	protected void addMarkers(ArrayList<Restaurant> restaurants) {
		ArrayList<OverlayItem> overlayItemArray = new ArrayList<OverlayItem>();
		for (Restaurant restaurant : restaurants) {
			overlayItemArray.add(new OverlayItem(restaurant.getName(), restaurant.getVicinity(), new GeoPoint(restaurant.getLatitude(),
					restaurant.getLongitude())));
		}
		ItemizedIconOverlay<OverlayItem> anotherItemizedIconOverlay = new ItemizedIconOverlay<OverlayItem>(this, overlayItemArray, null);
		openMapView.getOverlays().add(anotherItemizedIconOverlay);
		openMapView.invalidate();
	}
}
