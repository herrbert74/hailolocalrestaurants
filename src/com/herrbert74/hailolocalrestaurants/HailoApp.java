package com.herrbert74.hailolocalrestaurants;

import org.osmdroid.util.GeoPoint;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;

//http://www.devahead.com/blog/2011/06/extending-the-android-application-class-and-dealing-with-singleton/
//Don't forget to add a reference in the Manifest file!!!

/**
 * The Class HailoApp.
 * 
 * The Application class provides global data in singleton format. 
 */
public class HailoApp extends Application {

	/** The Constant TAG. */
	private static final String TAG = HailoApp.class.getSimpleName();

	/** The HailoApp instance. */
	private static HailoApp instance;

	/** The fonts. */
	private static Typeface mFont_headers, mFont_buttons;

	/** The is offline. */
	private static boolean isOffline = false;

	/** The Constant TYPEFACE_HEADINGS. */
	static final String TYPEFACE_HEADINGS = "CallaniAAHunBold.ttf";

	/** The Constant TYPEFACE_BUTTONS. */
	static final String TYPEFACE_BUTTONS = "Diavlo_LIGHT_II_37.otf";

	/** The Constant LOCATION_API_URL. */
	static final String LOCATION_API_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";

	/** The Constant LOCATION_API_PARAMETERS. */
	static final String LOCATION_API_PARAMETERS = "&radius=1641&types=restaurant&sensor=true&key=";

	/** The Constant OfflineJSONFileName. */
	static final String OfflineJSONFileName = "offline.json";

	/** The current location. */
	private static GeoPoint currentLocation;

	/** The Constant LOCATION_API_KEY. */
	static final String LOCATION_API_KEY = "AIzaSyC65y2lQl5SLEd46javbZrScarAm86XZoo";

	/* (non-Javadoc)
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		isOfflineAtStartup();
		initSingletons();
	}

	public static Context getContext() {
		return instance.getApplicationContext();
	}

	/**
	 * Inits the singletons.
	 */
	protected void initSingletons() {
		mFont_headers = Typeface.createFromAsset(getAssets(), TYPEFACE_HEADINGS);
		mFont_buttons = Typeface.createFromAsset(getAssets(), TYPEFACE_BUTTONS);
	}

	public static GeoPoint getCurrentLocation() {
		return currentLocation;
	}

	/**
	 * Sets the curretn location and saves it in SharedPreferences
	 * 
	 * @param currentLocation
	 */
	public static void setCurrentLocation(GeoPoint currentLocation) {
		HailoApp.currentLocation = currentLocation;
		SharedPreferences settings = getContext().getSharedPreferences("HailoApp", 0);
		SharedPreferences.Editor editor;
		editor = settings.edit();
		editor.putInt("latitude", currentLocation.getLatitudeE6());
		editor.putInt("longitude", currentLocation.getLongitudeE6());
		editor.commit();
	}

	public static String getOfflineJsonFilename() {
		return OfflineJSONFileName;
	}

	public static String getLocationApiUrl() {
		return LOCATION_API_URL;
	}

	public static String getLocationApiParameters() {
		return LOCATION_API_PARAMETERS;
	}

	public static String getLocationApiKey() {
		return LOCATION_API_KEY;
	}

	public static Typeface getFontHeaders() {
		return mFont_headers;
	}

	public static Typeface getFontButtons() {
		return mFont_buttons;
	}

	public static boolean isOffline() {
		return isOffline;
	}

	/**
	 * Checks if offline mode is on at startup.
	 */
	public static void isOfflineAtStartup() {
		SharedPreferences settings = getContext().getSharedPreferences("HailoApp", 0);
		HailoApp.isOffline = settings.getBoolean("isOffline", false);
		HailoApp.setCurrentLocation(new GeoPoint(settings.getInt("latitude", 0), settings.getInt("longitude", 0)));
	}

	/**
	 * Sets offline mode and saves the change in SharedPreferences
	 * 
	 * @param isOffline
	 */
	public static void setOffline(boolean isOffline) {
		HailoApp.isOffline = isOffline;
		SharedPreferences settings = getContext().getSharedPreferences("HailoApp", 0);
		SharedPreferences.Editor editor;
		editor = settings.edit();
		editor.putBoolean("isOffline", isOffline);
		editor.commit();
	}

}
